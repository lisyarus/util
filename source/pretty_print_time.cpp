#include <util/pretty_print.hpp>

namespace util
{

	namespace detail
	{

		void pretty_print_time (std::ostream & o, std::int64_t d, std::int64_t up_to)
		{
			static constexpr const std::int64_t durations[8] = {
				604800 * 1000000000ull,
				86400 * 1000000000ull,
				3600 * 1000000000ull,
				60 * 1000000000ull,
				1000000000ull,
				1000000ull,
				1000ull,
				1ull
			};

			static const std::string_view suffixes[8] = {
				"w",
				"d",
				"h",
				"m",
				"s",
				"ms",
				"us",
				"ns"
			};

			bool first = true;

			for (std::size_t i = 0; i < 8; ++i)
			{
				if (up_to > durations[i]) break;

				std::int64_t value = d / durations[i];
				d %= durations[i];

				if (value == 0) continue;

				if (first)
					first = false;
				else
					o << ' ';

				o << value << suffixes[i];
			}

			// [ first == true ] means no output was generated
			// Find first matching suffix and output zero
			if (first) for (std::size_t i = 0; i < 8; ++i)
			{
				if (up_to >= durations[i])
				{
					o << 0 << suffixes[i];
					break;
				}
			}
		}

	}

}
