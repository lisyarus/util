#include <util/unicode.hpp>

#include <codecvt>
#include <locale>

namespace util
{

	using converter = std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>;

	std::string to_utf8 (std::u32string const & str)
	{
		return converter().to_bytes(str);
	}

	std::u32string from_utf8 (std::string const & str)
	{
		return converter().from_bytes(str);
	}

}
