#include <util/not_implemented.hpp>

#include <stdexcept>

namespace util
{

	void not_implemented ( )
	{
		throw std::runtime_error("Not implemented");
	}

}
