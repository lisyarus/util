#include <util/threadpool.hpp>

namespace util
{

	void threadpool::start (std::size_t thread_count)
	{
		for (std::size_t th = 0; th < thread_count; ++th)
		{
			threads.emplace_back([this]
			{
				while (true)
				{
					auto task = tasks_queue.pop();

					if (!task)
						break;

					task();
				}
			});
		}
	}

	void threadpool::stop ( )
	{
		tasks_queue.clear();
		for (auto const & thread: threads)
		{
			(void)thread;
			tasks_queue.push({});
		}
		threads.clear();
	}

}
