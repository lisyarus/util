#include <util/movable_function.hpp>

#include <functional>

namespace util
{

	namespace detail
	{

		void bad_function_call ( )
		{
			throw std::bad_function_call();
		}

	}

}
