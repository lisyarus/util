#pragma once

#include <util/clock.hpp>

namespace util
{

	template <typename Duration = std::chrono::duration<double>, typename Clock = std::chrono::system_clock>
	struct timer
		: clock<Duration, Clock>
	{
		timer (Duration duration)
			: duration_(duration)
		{ }

		explicit operator bool ( )
		{
			if (this->duration() >= duration_)
			{
				this->restart();
				return true;
			}

			return false;
		}

	private:
		Duration duration_;
	};

}
