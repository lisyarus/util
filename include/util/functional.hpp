#pragma once

namespace util
{

	constexpr auto nop = [](auto const & ...){};

	constexpr auto id = [](auto && x) -> decltype(auto) { return x; };

	template <typename T>
	auto constant (T const & x)
	{
		return [x](auto const & ...){ return x; };
	}

	template <typename F1, typename F2>
	auto bind_and (F1 && f1, F2 && f2)
	{
		return [=](auto const &... args){
			return f1(args...) && f2(args...);
		};
	}

}
