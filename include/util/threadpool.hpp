#pragma once

#include <util/thread.hpp>
#include <util/synchronyzed_queue.hpp>
#include <util/movable_function.hpp>

#include <future>
#include <vector>

namespace util
{

	struct threadpool
	{
		threadpool ( )
			: threadpool(std::max(1u, std::thread::hardware_concurrency()))
		{ }

		threadpool (std::size_t thread_count)
		{
			start(thread_count);
		}

		~ threadpool ( )
		{
			stop();
		}

		template <typename F>
		auto dispatch (F && f)
		{
			using R = decltype(f());

			std::packaged_task<R()> task { std::forward<F>(f) };

			auto result = task.get_future();

			tasks_queue.push(std::move(task));

			return result;
		}

		void start (std::size_t thread_count);

		void stop ( );

		void wait ( )
		{
			tasks_queue.wait();
		}

	private:
		std::vector<util::thread> threads;
		util::synchronized_queue<movable_function<void()>> tasks_queue;
	};

}
