#pragma once

#include <deque>
#include <mutex>
#include <condition_variable>
#include <optional>
#include <thread>
#include <chrono>

namespace util
{

	template <typename T>
	struct synchronized_queue
	{
		synchronized_queue (std::size_t max_size = std::numeric_limits<std::size_t>::max()) noexcept
			: max_size_(max_size)
		{ }

		std::size_t max_size ( ) const noexcept
		{
			return max_size_;
		}

		void push (T const & x);
		void push (T && x);
		T pop ( );

		bool try_push (T const & x);
		template <typename Rep, typename Period>
		bool try_push (T const & x, std::chrono::duration<Rep, Period> const & timeout);

		std::optional<T> try_pop ( );
		template <typename Rep, typename Period>
		std::optional<T> try_pop (std::chrono::duration<Rep, Period> const & timeout);

		void clear ( );

		// Wait for the queue to become empty
		// e.g. when no new items are going to be pushed
		void wait ( );

	private:
		std::mutex mutex;
		std::condition_variable push_cv, pop_cv;
		std::deque<T> queue;
		std::size_t const max_size_;
	};

	template <typename T>
	void synchronized_queue<T>::push (T const & x)
	{
		std::unique_lock lock { mutex };
		push_cv.wait(lock, [this]{ return queue.size() < max_size(); });
		queue.push_back(x);
		pop_cv.notify_one();
	}

	template <typename T>
	void synchronized_queue<T>::push (T && x)
	{
		std::unique_lock lock { mutex };
		push_cv.wait(lock, [this]{ return queue.size() <  max_size(); });
		queue.push_back(std::move(x));
		pop_cv.notify_one();
	}

	template <typename T>
	T synchronized_queue<T>::pop ( )
	{
		std::unique_lock lock { mutex };
		pop_cv.wait(lock, [this]{ return !queue.empty(); });
		T x = std::move(queue.front());
		queue.pop_front();
		push_cv.notify_one();
		return x;
	}

	template <typename T>
	bool synchronized_queue<T>::try_push (T const & x)
	{
		std::lock_guard lock { mutex };
		if (queue.size() >=  max_size())
			return false;

		queue.push_back(x);
		pop_cv.notify_one();
		return true;
	}

	template <typename T>
	template <typename Rep, typename Period>
	bool synchronized_queue<T>::try_push (T const & x, std::chrono::duration<Rep, Period> const & timeout)
	{
		std::unique_lock lock { mutex };
		if (push_cv.wait_for(lock, timeout, [this]{ return queue.size() <  max_size(); }))
		{
			queue.push_back(std::move(x));
			pop_cv.notify_one();
			return true;
		}
		return false;
	}

	template <typename T>
	std::optional<T> synchronized_queue<T>::try_pop ( )
	{
		std::lock_guard lock { mutex };
		if (queue.empty())
			return std::nullopt;
		T x = std::move(queue.front());
		queue.pop_front();
		push_cv.notify_one();
		return { std::move(x) };
	}

	template <typename T>
	template <typename Rep, typename Period>
	std::optional<T> synchronized_queue<T>::try_pop (std::chrono::duration<Rep, Period> const & timeout)
	{
		std::unique_lock lock { mutex };
		if (pop_cv.wait_for(lock, timeout, [this]{ return !queue.empty(); }))
		{
			T x = std::move(queue.front());
			queue.pop_front();
			push_cv.notify_one();
			return { std::move(x) };
		}
		return std::nullopt;
	}

	template <typename T>
	void synchronized_queue<T>::clear ( )
	{
		std::lock_guard lock { mutex };
		queue.clear();
		push_cv.notify_all();
	}

	template <typename T>
	void synchronized_queue<T>::wait ( )
	{
		std::unique_lock lock { mutex };
		push_cv.wait(lock, [this]{ return queue.empty(); });
	}

}
