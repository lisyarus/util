#pragma once

#include <util/range.hpp>

#ifdef UTIL_USE_BOOST
	#include <boost/optional.hpp>
#endif

#if __cplusplus > 201700
	#include <optional>
#endif

namespace util
{

	namespace detail
	{

		template <typename F>
		struct fmap
		{
			F f;

#ifdef UTIL_USE_BOOST
			template <typename T>
			auto operator() (boost::optional<T> && x)
			{
				using R = decltype(f(*x));

				if (x)
					return boost::optional<R>(f(*x));
				else
					return boost::optional<R>();
			}
#endif

#if __cplusplus > 201700
			template <typename T>
			auto operator() (std::optional<T> && x)
			{
				using R = decltype(f(*x));

				if (x)
					return std::optional<R>(f(*x));
				else
					return std::optional<R>();
			}
#endif
		};

	}

	template <typename F>
	auto fmap (F f)
	{
		return detail::fmap<F>{std::move(f)};
	}

}
