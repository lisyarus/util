#pragma once

#include <iterator>
#include <utility>

namespace util_detail
{

	template <typename Container>
	auto begin_helper (Container & x)
	{
		using std::begin;
		return begin(x);
	}

	template <typename Container>
	auto end_helper (Container & x)
	{
		using std::end;
		return end(x);
	}

}

namespace util
{

	template <typename Container>
	auto begin (Container & x)
	{
		return util_detail::begin_helper(x);
	}

	template <typename Container>
	auto end (Container & x)
	{
		return util_detail::end_helper(x);
	}

	template <typename T>
	struct range_traits
	{
		using iterator = decltype(begin(std::declval<T>()));
		using iterator_traits = std::iterator_traits<iterator>;

		using iterator_category = typename iterator_traits::iterator_category;
		using value_type        = typename iterator_traits::value_type       ;
		using difference        = typename iterator_traits::difference       ;
		using pointer           = typename iterator_traits::pointer          ;
		using reference         = typename iterator_traits::reference        ;
	};

	template <typename Iterator>
	struct range
	{
		Iterator begin_, end_;

		Iterator begin() const { return begin_; }
		Iterator end() const { return end_; }
	};

	template <typename Iterator>
	range(Iterator, Iterator) -> range<Iterator>;

	template <typename Range>
	auto reversed(Range const & r)
	{
		auto rbegin = std::reverse_iterator{end(r)};
		auto rend = std::reverse_iterator{begin(r)};
		return range{rbegin, rend};
	}

}
