#pragma once

#include <array>

namespace util
{

	namespace detail
	{
		template <typename Type, std::size_t ... Sizes>
		struct multidimentional_array_impl;

		template <typename Type, std::size_t FirstSize, std::size_t ... Sizes>
		struct multidimentional_array_impl<Type, FirstSize, Sizes...>
		{
			typedef std::array<typename multidimentional_array_impl<Type, Sizes...>::type, FirstSize> type;
		};

		template <typename Type>
		struct multidimentional_array_impl<Type>
		{
			typedef Type type;
		};
	}

	template <typename Type, std::size_t ... Sizes>
	using multidimentional_array = typename detail::multidimentional_array_impl<Type, Sizes...>::type;

}
