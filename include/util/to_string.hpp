#pragma once

#include <sstream>
#include <stdexcept>

namespace util
{

	namespace detail
	{

		template <typename Char, typename Traits = std::char_traits<Char>, typename ... Args>
		std::basic_string<Char, Traits> to_string (Args const & ... args)
		{
			std::basic_ostringstream<Char, Traits> oss;

			((oss << args), ...);

			return oss.str();
		}

	}

	template <typename ... Args>
	std::string to_string (Args const & ... args)
	{
		return detail::to_string<char>(args...);
	}

	template <typename ... Args>
	std::wstring to_wstring (Args const & ... args)
	{
		return detail::to_string<wchar_t>(args...);
	}

	template <typename ... Args>
	std::u32string to_u32string (Args const & ... args)
	{
		return detail::to_string<char32_t>(args...);
	}

	template <typename T, typename Char, typename Traits>
	T from_string (std::basic_string<Char, Traits> const & s)
	{
		std::basic_istringstream<Char, Traits> iss(s);
		T x;
		iss >> x;
		if (!iss)
			throw std::invalid_argument("failed to parse from string");
		return x;
	}

	template <typename T, typename Char>
	T from_string (Char const * s)
	{
		return from_string<T, Char, std::char_traits<Char>>(s);
	}

}
