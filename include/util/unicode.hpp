#pragma once

#include <string>

namespace util
{

	std::string to_utf8 (std::u32string const & str);
	std::u32string from_utf8 (std::string const & str);

}
