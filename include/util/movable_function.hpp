#pragma once

#include <memory>

namespace util
{

	namespace detail
	{

		template <typename Signature>
		struct movable_function_node_base;

		template <typename R, typename ... Args>
		struct movable_function_node_base<R(Args...)>
		{
			virtual R call (Args ...) = 0;

			virtual ~ movable_function_node_base ( ) = default;
		};

		template <typename Signature, typename F>
		struct movable_function_node;

		template <typename R, typename ... Args, typename F>
		struct movable_function_node<R(Args...), F>
			: movable_function_node_base<R(Args...)>
		{
			F f;

			movable_function_node (F && f)
				: f(std::move(f))
			{ }

			R call (Args ... args) override
			{
				return f(args...);
			}
		};

		// Implemented in cpp to prevent dependency on <functional>
		[[noreturn]] void bad_function_call ( );

	}

	template <typename Signature>
	struct movable_function;

	template <typename R, typename ... Args>
	struct movable_function<R(Args...)>
	{
		using signature = R(Args...);

		movable_function ( ) = default;

		template <typename F>
		movable_function (F f)
			: p { std::make_unique<detail::movable_function_node<signature, std::remove_cv_t<F>>>(std::move(f)) }
		{ }

		movable_function (movable_function &&) = default;
		movable_function & operator = (movable_function &&) = default;

		movable_function (movable_function const &) = delete;
		movable_function & operator = (movable_function const &) = delete;

		explicit operator bool ( ) const
		{
			return static_cast<bool>(p);
		}

		R operator() (Args ... args)
		{
			if (!p)
				detail::bad_function_call();

			return p->call(args...);
		}

	private:
		std::unique_ptr<detail::movable_function_node_base<signature>> p;
	};

}
