#pragma once

namespace util
{

	namespace detail
	{

		template <typename F>
		struct recursive_impl
		{
			F f;

			template <typename ... Args>
			auto operator() (Args && ... args) -> decltype(auto)
			{
				return f(*this, std::forward<Args>(args)...);
			}
		};

	}

	template <typename F>
	auto recursive (F f)
	{
		return detail::recursive_impl<F>{std::move(f)};
	}

}
