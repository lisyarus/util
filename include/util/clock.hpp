#pragma once

#include <chrono>

namespace util
{

	template <typename Duration = std::chrono::duration<double>, typename Clock = std::chrono::system_clock>
	struct clock
	{
		typedef Duration duration_type;
		typedef typename duration_type::rep rep_type;

		typedef Clock clock_type;
		typedef typename clock_type::time_point time_point_type;

		clock ( )
		{
			restart();
		}

		time_point_type now ( ) const
		{
			return clock_type::now();
		}

		duration_type restart ( )
		{
			auto t = now();
			auto delta = t - start_;
			start_ = t;
			return std::chrono::duration_cast<duration_type>(delta);
		}

		duration_type duration ( ) const
		{
			return std::chrono::duration_cast<duration_type>(now() - start_);
		}

		rep_type count ( ) const
		{
			return duration().count();
		}

	private:
		time_point_type start_;
	};

}
