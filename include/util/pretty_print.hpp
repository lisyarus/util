#pragma once

#include <chrono>
#include <iostream>

namespace util
{

	namespace detail
	{

		template <typename Duration, typename UpTo>
		struct pretty_print_time_wrapper
		{
			Duration d;
			UpTo up_to;
		};

		void pretty_print_time (std::ostream & o, std::int64_t d, std::int64_t up_to);

		template <typename Duration, typename UpTo>
		std::ostream & operator << (std::ostream & o, pretty_print_time_wrapper<Duration, UpTo> t)
		{
			std::int64_t const d = std::chrono::duration_cast<std::chrono::nanoseconds>(t.d).count();
			std::int64_t const up_to = std::chrono::duration_cast<std::chrono::nanoseconds>(t.up_to).count();
			pretty_print_time(o, d, up_to);
			return o;
		}

	}

	template <typename Rep, typename Period, typename UpTo>
	auto pretty (std::chrono::duration<Rep, Period> d, UpTo up_to)
	{
		return detail::pretty_print_time_wrapper<std::chrono::duration<Rep, Period>, UpTo>{d, up_to};
	}

	template <typename Rep, typename Period>
	auto pretty (std::chrono::duration<Rep, Period> d)
	{
		return pretty(d, std::chrono::seconds{1});
	}

}
