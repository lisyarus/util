#pragma once

#include <memory>
#include <string>
#include <string_view>

namespace util
{

	struct blob
	{
		blob() = default;
		blob(blob const & other);
		blob(blob && other);

		blob(std::size_t size);
		blob(std::size_t size, std::unique_ptr<char[]> data);
		blob(std::size_t size, char * data);

		blob & operator = (blob const & other);
		blob & operator = (blob && other);

		~blob() = default;

		char * data() { return data_.get(); }
		char const * data() const { return data_.get(); }

		std::size_t size() const { return size_; }

		explicit operator bool() const { return static_cast<bool>(data_); }

		void reset();
		std::unique_ptr<char[]> release();

		void swap(blob & other);

		using iterator = char *;
		using const_iterator = char const *;

		char * begin() { return data(); }
		char const * begin() const { return data(); }

		char * end() { return data() + size(); }
		char const * end() const { return data() + size(); }

		char & operator[](std::size_t i) { return data()[i]; }
		char const & operator[](std::size_t i) const { return data()[i]; }

		std::string string() const;
		std::string_view string_view() const;

	private:
		std::unique_ptr<char[]> data_;
		std::size_t size_ = 0;
	};

	inline blob::blob(blob const & other)
	{
		*this = other;
	}

	inline blob::blob(blob && other)
		: data_{std::move(other.data_)}
		, size_{other.size_}
	{
		other.size_ = 0;
	}

	inline blob::blob(std::size_t size)
		: data_{new char [size]}
		, size_{size}
	{}

	inline blob::blob(std::size_t size, std::unique_ptr<char[]> data)
		: data_{std::move(data)}
		, size_{size}
	{}

	inline blob::blob(std::size_t size, char * data)
		: data_{data}
		, size_{size}
	{}

	inline blob & blob::operator=(blob const & other)
	{
		if (this != &other)
		{
			data_.reset(new char [other.size()]);
			std::copy(other.begin(), other.end(), begin());
			size_ = other.size();
		}
		return *this;
	}

	inline blob & blob::operator=(blob && other)
	{
		blob(std::move(other)).swap(*this);
		return *this;
	}

	inline void blob::reset()
	{
		data_.reset();
		size_ = 0;
	}

	inline std::unique_ptr<char[]> blob::release()
	{
		size_ = 0;
		return std::move(data_);
	}

	inline void blob::swap(blob & other)
	{
		std::swap(data_, other.data_);
		std::swap(size_, other.size_);
	}

	inline std::string blob::string() const
	{
		return std::string(data_.get(), data_.get() + size_);
	}

	inline std::string_view blob::string_view() const
	{
		return std::string_view(data_.get(), size_);
	}
}
