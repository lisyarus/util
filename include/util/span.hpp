#pragma once

#include <iterator>
#include <array>

namespace util
{

	template <typename T>
	struct span
	{
		using value_type = std::remove_cv_t<T>;
		using size_type = std::size_t;
		using difference_type = std::ptrdiff_t;
		using pointer = T *;
		using const_pointer = const T *;
		using reference = T &;
		using const_reference = const T &;
		using iterator = pointer;
		using reverse_iterator = std::reverse_iterator<iterator>;

		T * p_begin = nullptr;
		T * p_end = nullptr;

		span() = default;
		span(span const &) = default;
		span(span &&) = default;

		span(T * begin, T * end)
			: p_begin{begin}
			, p_end{end}
		{}

		template <std::size_t N>
		span(T (&a)[N])
			: p_begin{a}
			, p_end{a + N}
		{}

		template <std::size_t N>
		span(std::array<T, N> & a)
			: p_begin{a.data()}
			, p_end{a.data() + N}
		{}

		T * data() const { return p_begin; }

		T * begin() const { return p_begin; }
		T * end() const { return p_end; }

		difference_type size() const { return p_end - p_begin; }

		reverse_iterator rbegin() const { return reverse_iterator(p_end); }
		reverse_iterator rend() const { return reverse_iterator(p_begin); }

		T & operator[] (size_type i) const { return p_begin[i]; }
	};

}
