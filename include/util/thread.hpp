#pragma once

#include <thread>

namespace util
{

	struct thread
		: std::thread
	{
		template <typename ... Args>
		thread (Args && ... args)
			: std::thread(std::forward<Args>(args)...)
		{ }

		thread (thread &&) = default;

		~ thread ( )
		{
			if (joinable())
				join();
		}
	};

}
