#pragma once

namespace util
{

	struct noncopyable
	{
		noncopyable (noncopyable const &)              = delete;
		noncopyable & operator = (noncopyable const &) = delete;

		noncopyable ( )                           = default;
		noncopyable (noncopyable &&)              = default;
		noncopyable & operator = (noncopyable &&) = default;
	};

}
