#pragma once

#include <type_traits>

namespace util::pimpl
{

	template <typename Parent>
	struct impl;

	template <typename Parent, std::size_t Size>
	struct base
	{
	private:

		typename std::aligned_storage<Size>::type storage_;

	public:

		using impl_type = ::util::pimpl::impl<Parent>;

		impl_type * pimpl ( ) { return reinterpret_cast<impl_type *>(&storage_); }
		impl_type & impl  ( ) { return *pimpl(); }

		impl_type const * pimpl ( ) const { return reinterpret_cast<impl_type const *>(&storage_); }
		impl_type const & impl  ( ) const { return *pimpl(); }

		template <typename ... Args>
		base (Args && ... args)
		{
			static_assert(sizeof(impl_type) <= Size, "impl storage size too small");
			new (pimpl()) impl_type (std::forward<Args>(args)...);
		}

		~ base ( )
		{
			impl().~impl_type();
		}
	};

}
