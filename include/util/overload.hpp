#pragma once

#include <utility>

namespace util
{

	namespace detail
	{

		template <typename ... Fs>
		struct overload_impl
			: Fs...
		{
			overload_impl (Fs ... fs)
				: Fs(fs)...
			{ }

			using Fs::operator()...;
		};

	}

	template <typename ... Fs>
	auto overload (Fs ... fs)
	{
		return detail::overload_impl<Fs...>{std::move(fs)...};
	}

}
