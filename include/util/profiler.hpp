#pragma once

#include <util/clock.hpp>
#include <util/pretty_print.hpp>

#include <string>

namespace util
{

	struct profiler
	{
		profiler (std::string name)
			: name_(std::move(name))
		{ }

		~ profiler ( )
		{
			std::cout << name_ << ": " << util::pretty(clock_.duration(), std::chrono::microseconds{1}) << "\n";
		}

	private:
		std::string const name_;
		util::clock<> clock_;
	};

}
