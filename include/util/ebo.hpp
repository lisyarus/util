#pragma once

namespace util
{

	namespace detail
	{

		template <typename T, bool CanInherit>
		struct ebo_helper_impl : T
		{
			T & data() { return *this; }
			T const & data() const { return *this; }

			template <typename ... Args>
			ebo_helper_impl(Args && ... args)
				: T(std::forward<Args>(args)...)
			{}
		};

		template <typename T>
		struct ebo_helper_impl<T, false>
		{
			T value;

			T & data() { return value; }
			T const & data() const { return value; }

			template <typename ... Args>
			ebo_helper_impl(Args && ... args)
				: value(std::forward<Args>(args)...)
			{}
		};

	}

	template <typename T>
	using ebo_helper = detail::ebo_helper_impl<T, std::is_class_v<T> && !(std::is_final_v<T>)>;

}
